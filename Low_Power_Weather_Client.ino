#include <FastIO.h>
#include "printf.h"


#include <BMP085.h>
#include "DHT.h"
#include <Wire.h>
#include <OneWire.h>
#include <DallasTemperature.h>


#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include <RF24Network.h>


#include "LowPower.h"
#include "system.h"
System sys;

#define DEDUG;




//-------------------------------------DS18B20----------------------------------------------
#define DEDUG_SERIAL_TEMPERATURE;


OneWire ds(8);      //Создаем датчик DS18B20 на 8 пине
DallasTemperature sensors(&ds);

DeviceAddress tempDeviceAddress;

int  resolution = 12; //8,9
unsigned long lastTempRequest = 0;

//-------------------------------------DS18B20----------------------------------------------

//-------------------------------------LED_Setup--------------------------------------------
const int LEDpin = 5;
#define LED_ON digitalWrite(LEDpin, HIGH);
#define LED_OFF digitalWrite(LEDpin, LOW);
//-------------------------------------LED_Setup--------------------------------------------

//-------------------------------------BMP085-----------------------------------------------
BMP085 dps = BMP085();      // Digital Pressure Sensor

//long Temperature = 0, Pressure = 0, Altitude = 0;
//-------------------------------------BMP085-----------------------------------------------

//-------------------------------------DHT--------------------------------------------------
// Connect pin 1 (on the left) of the sensor to +5V
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor
DHT dht;
//-------------------------------------DHT--------------------------------------------------

//-------------------------------------Light_Analog-----------------------------------------
int photoPin = 0;  // фоторезистор подключен 0-му аналоговому входу

//-------------------------------------Light_Analog-----------------------------------------

//---------------------------------Struct_Sensor--------------------------------------------
struct Sensors
{
	float BMP_Altitude;          // Alt(m): id           //ID: 17
	float BMP_Pressure;          // Pressure(mm Hg):     //ID: 15
	float BMP_Temperature;     // Temperature(C);      //ID: 16
	float DHT_humidity;        // Humidity(%);         //ID: 7
	float DHT_temperature;     // Temperature(C);      //ID: 6
	int Light_Analog;
	float temperature_DS;      // Temperature(C);      //ID: 2
	float voltage_node;        // Volt (V)             //ID: 18
	
	int temp_Lost_packets;
	float Lost_packets;            //Lost_packets          //ID:240
	int Send_packets;
	
	int Free_RAM;
	char* Uptime;
}
F_struct_2;
//---------------------------------Struct_Sensor--------------------------------------------



//-----------------------------------nRF24L01------------------------------------------------
struct payload_t
{
	byte type;                         //<< Тип датчика
	byte number;                       //<< Номер датчика
	int value;                         //<< Значения датчика
}
F_tt;

RF24 radio(9,10);
RF24Network network(radio);
/*
Node 00 is the base node.
Nodes 01-05 are nodes whose parent is the base.
Node 021 is the second child of node 01.
Node 0321 is the third child of node 021, an so on.
The largest node address is 05555, so 3,125 nodes are allowed on a single channel.
*/

// Адрес нашего узла
const uint16_t this_node = 01;

// Адрес приемника
const uint16_t to = 00;
//-----------------------------------nRF24L01------------------------------------------------

//------------------------------StreamPrint------------------------------------------
#define Serialprint(format, ...) StreamPrint_progmem(Serial,PSTR(format),##__VA_ARGS__)
#define Streamprint(stream,format, ...) StreamPrint_progmem(stream,PSTR(format),##__VA_ARGS__)

//------------------------------StreamPrint------------------------------------------
void StreamPrint_progmem(Print &out,PGM_P format,...)
{
	// program memory version of printf - copy of format string and result share a buffer
	// so as to avoid too much memory use
	char formatString[128], *ptr;
	strncpy_P( formatString, format, sizeof(formatString) ); // copy in from program mem
	// null terminate - leave last char since we might need it in worst case for result's \0
	formatString[ sizeof(formatString)-2 ]='\0';
	ptr=&formatString[ strlen(formatString)+1 ]; // our result buffer...
	va_list args;
	va_start (args,format);
	vsnprintf(ptr, sizeof(formatString)-1-strlen(formatString), formatString, args );
	va_end (args);
	formatString[ sizeof(formatString)-1 ]='\0';
	out.print(ptr);
}




