/* 
	Editor: http://www.visualmicro.com
	        arduino debugger, visual micro +, free forum and wiki
	
	Hardware: Arduino Pro or Pro Mini w/ ATmega328 (5V, 16 MHz), Platform=avr, Package=arduino
*/

#define __AVR_ATmega328P__
#define ARDUINO 101
#define ARDUINO_MAIN
#define F_CPU 16000000L
#define __AVR__
#define __cplusplus
extern "C" void __cxa_pure_virtual() {;}

//
//
void Get_18B20_Data();
void Update_Voltage_BAttery();
void  Update_Variable_BMP();
void  Update_Variable_DHT();
void Update_variable_Light_Analog();
void Update_System();
void View_to_Serial();
void Send_base_to_mesage_on_node();
bool send_M(uint16_t to);
long readVcc();

#include "D:\util\arduino-1.5.2\hardware\arduino\avr\variants\standard\pins_arduino.h" 
#include "D:\util\arduino-1.5.2\hardware\arduino\avr\cores\arduino\arduino.h"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Автономная_погодная_станция\source\Low_Power_Weather_Client\Low_Power_Weather_Client.ino"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Автономная_погодная_станция\source\Low_Power_Weather_Client\A01_void_Setup.ino"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Автономная_погодная_станция\source\Low_Power_Weather_Client\A02_void_Loop.ino"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Автономная_погодная_станция\source\Low_Power_Weather_Client\A12_Get_18B20_Data.ino"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Автономная_погодная_станция\source\Low_Power_Weather_Client\A15_Utils.ino"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Автономная_погодная_станция\source\Low_Power_Weather_Client\System.cpp"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Автономная_погодная_станция\source\Low_Power_Weather_Client\System.h"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Автономная_погодная_станция\source\Low_Power_Weather_Client\eeprom_update_block.h"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Автономная_погодная_станция\source\Low_Power_Weather_Client\printf.h"
