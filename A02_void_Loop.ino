

void loop() {
  //----------------------------Setup_Low_Power--------------------------------------
  // ATmega328P, ATmega168
  //LowPower.idle(SLEEP_8S, ADC_OFF, TIMER2_OFF, TIMER1_OFF, TIMER0_OFF, 
  //              SPI_OFF, USART0_OFF, TWI_OFF);
  // ATmega32U4
  //LowPower.idle(SLEEP_8S, ADC_OFF, TIMER4_OFF, TIMER3_OFF, TIMER1_OFF, 
  //		  TIMER0_OFF, SPI_OFF, USART1_OFF, TWI_OFF, USB_OFF);

  // ATmega2560
  //LowPower.idle(SLEEP_8S, ADC_OFF, TIMER5_OFF, TIMER4_OFF, TIMER3_OFF, 
  //		  TIMER2_OFF, TIMER1_OFF, TIMER0_OFF, SPI_OFF, USART3_OFF, 
  //		  USART2_OFF, USART1_OFF, USART0_OFF, TWI_OFF);
  /*   
   LowPower.idle(SLEEP_8S, ADC_OFF, TIMER2_OFF, TIMER1_OFF, TIMER0_OFF, 
   SPI_ON, USART0_ON, TWI_OFF);  
   */

  for (uint8_t i = 0; i < 38; i++) {//интервал отправки  304 секунды (5.06мин)//38
#ifdef DEDUG
    printf_P(PSTR("***   Sleep : %d sec.\n\r"),(i+1)*8);
#endif
    //  network.update();
    if (i == 37) {  

      //каждые 64(1.06 мин),128(2.13 мин),256(4.26 мин) секунды происходит обновление переменных (8*8=64 сек)

      //-------------------------------------DS18B20----------------------------------------------
      Get_18B20_Data();
      //-------------------------------------DS18B20----------------------------------------------

      //-------------------------------------BMP085-----------------------------------------------
      Update_Variable_BMP();
      //-------------------------------------BMP085-----------------------------------------------

      //-------------------------------------DHT--------------------------------------------------
      Update_Variable_DHT();
      //-------------------------------------DHT--------------------------------------------------

      //-------------------------------------Light_Analog-----------------------------------------
      Update_variable_Light_Analog();
      //-------------------------------------Light_Analog-----------------------------------------

      //-------------------------------------Update_Volt_on_Battery-------------------------------
      Update_Voltage_BAttery();
      //-------------------------------------Update_Volt_on_Battery-------------------------------
      
     //-------------------------------------System_Update----------------------------------------
     Update_System();
     //-------------------------------------System_Update----------------------------------------

      Update_System();
#ifdef DEDUG
      View_to_Serial();
#endif

    }
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF); 
  }
  Send_message();

}















