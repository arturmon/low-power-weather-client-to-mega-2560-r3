void Update_Voltage_BAttery(){
  /*
	analogReference(INTERNAL);
   	float sensorValue = analogRead(A6);
   	F_struct_2.voltage_node= (sensorValue * (5.0 / 1023.0))+0.75; //1.003 компенсация падения на диоде
   	*/
  unsigned int ADCValue;
  double Vcc;

  Vcc = readVcc()/1000.0;
  ADCValue = analogRead(A6);
  F_struct_2.voltage_node = (ADCValue / 1023.0) * Vcc;
  Serial.println(F_struct_2.voltage_node);
}

void  Update_Variable_BMP(){
  long BMP_Pressure_temp;
  long BMP_Altitude_temp;
  long BMP_Temperature_temp;
  dps.calcTrueTemperature();
  dps.getPressure(&BMP_Pressure_temp);
  dps.getAltitude(&BMP_Altitude_temp);
  dps.getTemperature(&BMP_Temperature_temp);
  F_struct_2.BMP_Pressure=BMP_Pressure_temp/133.3;
  F_struct_2.BMP_Altitude=BMP_Altitude_temp/100;
  F_struct_2.BMP_Temperature=BMP_Temperature_temp*0.1;
}


void  Update_Variable_DHT(){

  F_struct_2.DHT_humidity = dht.getHumidity();
  F_struct_2.DHT_temperature = dht.getTemperature();
  dht.getStatusString();//--------------------------------??????????????????
}


void Update_variable_Light_Analog(){
  int val = 0;       // переменная для хранения значения входного напряжения
  val = analogRead(photoPin);  // считываем значение с фоторезистора
  val = val/4;                 // конвертируем из 0-1023 к 0-255
  F_struct_2.Light_Analog = val;
}

void Update_System(){
  F_struct_2.Free_RAM=sys.ramFree();
  F_struct_2.Uptime=sys.uptime();
  F_struct_2.Lost_packets= float(F_struct_2.temp_Lost_packets)/(float(F_struct_2.Send_packets)/100)  ;
}

#ifdef DEDUG
void View_to_Serial(){
  printf_P(PSTR("  Altitude(m): "));
  Serial.print(F_struct_2.BMP_Altitude);
  printf_P(PSTR(" m  Pressure(mm Hg): "));
  Serial.print(F_struct_2.BMP_Pressure);
  printf_P(PSTR(" mm Hg  Temp: "));
  Serial.print(F_struct_2.BMP_Temperature);
  printf_P(PSTR(" C  Light: "));
  Serial.print(F_struct_2.Light_Analog);
  printf_P(PSTR("  DS18B20: "));
  Serial.print(F_struct_2.temperature_DS);
  printf_P(PSTR(" C\n\r"));
  printf_P(PSTR("  DHT humidity: "));
  Serial.print(F_struct_2.DHT_humidity);
  printf_P(PSTR(" %%  DHT Temp: "));
  Serial.print(F_struct_2.DHT_temperature);
  printf_P(PSTR(" C  Volt: "));
  Serial.print(F_struct_2.voltage_node);

  printf_P(PSTR(" V  Free RAM: "));
  Serial.print(F_struct_2.Free_RAM);
  printf_P(PSTR("  Uptime: "));
  Serial.println(F_struct_2.Uptime);
  printf_P(PSTR(" Lost pakcets: "));
  Serial.print(F_struct_2.Lost_packets);
}
#endif

void Send_message(){
  radio.powerUp();
  LED_ON;



  F_tt.value=F_struct_2.BMP_Altitude;
  F_tt.number=1;
  F_tt.type=17;

  Send_base_to_mesage_on_node();

  F_tt.value=F_struct_2.BMP_Pressure*10;
  //F_tt.value=(F_struct_2.BMP_Pressure*100)-60000;// 272,33-***** дохера )))
  F_tt.number=2;
  F_tt.type=15;

  Send_base_to_mesage_on_node();

  F_tt.value=F_struct_2.BMP_Temperature*100;
  F_tt.number=3;
  F_tt.type=16;

  Send_base_to_mesage_on_node();

  F_tt.value=F_struct_2.DHT_humidity*100;
  F_tt.number=4;
  F_tt.type=7;

  Send_base_to_mesage_on_node();

  F_tt.value=F_struct_2.DHT_temperature*100;
  F_tt.number=5;
  F_tt.type=6;

  Send_base_to_mesage_on_node();

  F_tt.value=F_struct_2.Light_Analog;
  F_tt.number=6;
  F_tt.type=14;

  Send_base_to_mesage_on_node();

  F_tt.value=F_struct_2.temperature_DS*100;
  F_tt.number=7;
  F_tt.type=2;

  Send_base_to_mesage_on_node();

  F_tt.value=F_struct_2.voltage_node*100;
  F_tt.number=8;
  F_tt.type=18;

  Send_base_to_mesage_on_node();  

  F_tt.value=F_struct_2.Free_RAM;
  F_tt.number=9;
  F_tt.type=200;

  Send_base_to_mesage_on_node(); 

  F_tt.value=F_struct_2.Lost_packets;
  F_tt.number=10;
  F_tt.type=240;

  Send_base_to_mesage_on_node(); 



  /*
  F_tt.value=F_struct_2.Uptime;
   F_tt.number=10;
   F_tt.type=201;
   
   Send_base_to_mesage_on_node();
   */
  LED_OFF; 
  radio.powerDown();  
}

void Send_base_to_mesage_on_node()
{
  bool ok;
  ok = send_M(to);

  if (ok)
  {
    if (F_struct_2.Send_packets < 32767){
      F_struct_2.Send_packets=F_struct_2.Send_packets++;
    }

    else{
      F_struct_2.Send_packets=0;
      F_struct_2.temp_Lost_packets=0;
    }
#ifdef DEDUG
    printf_P(PSTR("%lu: APP Sending ok\n\r"),millis());
#endif
  }
  else
  {
    if (F_struct_2.temp_Lost_packets < 32767){
      F_struct_2.temp_Lost_packets=F_struct_2.temp_Lost_packets++;
    }
    else{
      F_struct_2.Send_packets=0;
      F_struct_2.temp_Lost_packets=0;
    }
#ifdef DEDUG
    printf_P(PSTR("%lu: APP Sending failed\n\r"),millis());
#endif
  }

}






bool send_M(uint16_t to)
{

  RF24NetworkHeader header(/*to node*/ to, /*type*/ 'M' /*Time*/);
#ifdef DEDUG
  printf_P(PSTR("---------------------------------\n\r"));
  printf_P(PSTR("%lu: APP +++++ type ID %d number %d value %d to 0%d...\n\r"),millis(),F_tt.type,F_tt.number,F_tt.value,to);
#endif
  return network.write(header,&F_tt,sizeof(F_tt));
}



long readVcc() {
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
#if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
  ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
#elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
  ADMUX = _BV(MUX5) | _BV(MUX0);
#elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
  ADMUX = _BV(MUX3) | _BV(MUX2);
#else
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
#endif

  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring

  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH
  uint8_t high = ADCH; // unlocks both

  long result = (high<<8) | low;

  result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  return result; // Vcc in millivolts
}

