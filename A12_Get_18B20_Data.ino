
void Get_18B20_Data(){
#ifdef DEDUG_SERIAL_TEMPERATURE
  printf_P(PSTR("  Temperature: "));
#endif
  sensors.requestTemperatures();
  F_struct_2.temperature_DS = sensors.getTempCByIndex(0);
#ifdef DEDUG_SERIAL_TEMPERATURE
  Serial.println(F_struct_2.temperature_DS, resolution); 
  printf_P(PSTR("  Resolution: "));
  printf_P(PSTR("%d\n"),resolution); 
#endif
}


